#using JFVM, JFVMvis
#import Base: +, -, *, /, ^, ==, >, >=, <, <=, broadcast, sin, cos, tan, cot, abs, exp, log, log10, abs

using SparseArrays, PyPlot, JFVM
import Base: +,
             -,
             *,
             /,
             ^,
             ==,
             >,
             >=,
             <,
             <=,
             broadcast,
             sin,
             cos,
             tan,
             cot,
             abs,
             exp,
             log,
             log10,
             abs


include("VisualizePDE.jl")

Nx = 10
Lx = 1.0
dx = 1 / 12
k = 1 / (dx * dx)
#U = zeros(Nx, Lx)
#m = createMesh1D(Nx, Lx)
m = createMesh2D(Nx, Nx, Lx, Lx)
BC = createBC(m)
BC.right.a[:] .= 0.0
BC.left.a[:] .= 0.0
BC.left.b[:] = BC.right.b[:] .= 1.0
BC.left.c[:] .= 1.0
BC.right.c[:] .= 0.0
#c_init = 0.0 # initial value of the variable
c_old = createCellVariable(m, 0.0, BC)
U = createCellVariable(m, 0.0, BC)

D_val = 1.0 # value of the diffusion coefficient
D_cell = createCellVariable(m, D_val) # assigned to cells
# Harmonic average
D_face = harmonicMean(D_cell)
N_steps = 20 # number of time steps
dt = sqrt(Lx^2 / D_val) / N_steps # time step
M_diff = diffusionTerm(D_face) # matrix of coefficient for diffusion term
(M_bc, RHS_bc) = boundaryConditionTerm(BC) # matrix of coefficient and RHS for the BC
for i = 1:20
    global c_old
    (M_t, RHS_t) = transientTerm(c_old, dt, 1.0)
    M = M_t - M_diff + M_bc # add all the [sparse] matrices of coefficient
    RHS = RHS_bc + RHS_t # add all the RHS's together
    c_old = solveLinearPDE(m, M, RHS) # solve the PDE
end
#v = k*(-2*c_old.value[1, 3] + c_old.value[2, 3] + c_old.value[3, 3])
#print(v, " ")
for j = 2:11
    for i = 2:11
        global c_old, U
        U.value[i-1, j] = k *
                                (-2 * c_old.value[i, j] +
                                c_old.value[i-1, j] +
                                c_old.value[i+1, j])
       #print(" ",U.value[i-1])
   end
end   

print(U)
fig1 = figure(figsize = (5, 4))
visualizeCells(U)
colorbar()
display(fig1)